// Reusable function to prevent event propagation
export const stopProp = (e) => {
    e.stopPropagation();
}