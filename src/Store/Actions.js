import { types } from "./Reducer"


const useActions = (dispatch) => {
    const setUsers = (item) => {
        dispatch({type: types.SET_USERS, payload: item})
    }

    const setFilters = (item) => {
        dispatch({type: types.SET_FILTERS, payload: item})
    }

    return{
        setUsers,
        setFilters
    }
}

export default useActions