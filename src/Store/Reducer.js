const initialState = {
    // for storing user array retrieved from api call
    users: [],
    // for determining which filters to apply upon rendering user list
    filters: {
        search: '',
        sortBy: 'name'
    }
}

const types = {
    SET_USERS: "SET_USERS",
    SET_FILTERS: "SET_FILTERS"
}

const Reducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SET_USERS:
            return {
                ...state,
                users: action.payload
            }
        case types.SET_FILTERS: 
            return{
                ...state,
                filters: {
                    ...state.filters,
                    [action.payload.filterType]: action.payload.value
                }
            }
        default: return state
    }

}

export { Reducer, types, initialState }