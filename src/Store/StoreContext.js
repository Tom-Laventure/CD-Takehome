import React, { createContext, useEffect, useReducer } from 'react'
import useActions from './Actions'
import { initialState, Reducer } from './Reducer'

const StoreContext = createContext(initialState)



const StoreProvider = ({children}) => {
    const [state, dispatch] = useReducer(Reducer, initialState)
    const actions = useActions(dispatch)

    useEffect(() => {
        // console.log(state)
    }, [state])


    return(
        <StoreContext.Provider value={{state, actions}}>
            {children}
        </StoreContext.Provider>
    )
}

export {StoreContext, StoreProvider}