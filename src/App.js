import { Route, Switch } from "react-router";
import UserList from "./Containers/UserList/UserList";
import UserDetails from "./Containers/UserDetails/UserDetails";


function App() {
  return (
    <Switch>
      <Route path='/user-details/:id' component={UserDetails}/>
      <Route path='/' component={UserList}/>
    </Switch>
  );
}

export default App;
