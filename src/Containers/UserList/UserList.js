import React, { useContext, useEffect } from 'react'
import classes from './UserList.module.css'
import FilterSection from '../../Components/UserListComponents/FilterSection/FilterSection'
import UserSelection from '../../Components/UserListComponents/UserSelection/UserSelection'
import axiosInstance from '../../Assets/Axios/AxiosInstance'
import { StoreContext } from '../../Store/StoreContext'

// 
const UserList = () => {
    const { actions } = useContext(StoreContext)

    // Runs on every first render of component
    useEffect(() => {
        // Gets all users
        axiosInstance.get('/users').then(res => {
            // adds array of users to central state
            actions.setUsers(res.data)
        }).catch(err => {
            console.log(err)
        })
    }, [])

    return (
        <div className={classes.container}>
            <div className={classes.body}>
                <div className={classes.header}>
                    <h1>Users</h1>
                    <FilterSection />
                </div>
                <UserSelection />
            </div>
        </div>
    )
}

export default UserList