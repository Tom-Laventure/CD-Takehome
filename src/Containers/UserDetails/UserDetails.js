import React, { useEffect, useState } from 'react'
import { useParams, withRouter } from 'react-router'
import axiosInstance from '../../Assets/Axios/AxiosInstance'
import UserInfo from '../../Components/UserDetailsComponents/UserInfo/UserInfo'
import UserPosts from '../../Components/UserDetailsComponents/UserPosts/UserPosts'
import classes from './UserDetails.module.css'

const UserDetails = ({ history }) => {
    const [userDetails, setUserDetails] = useState({
        user: {},
        loading: true
    })
    let { id } = useParams()

    // runs on first render, and every render that userID changes
    useEffect(() => {
        axiosInstance.get(`/users/${id}`).then(res => {
            // if data is loaded successfully, modify user state with data and set loading to false
            setUserDetails(old => {
                let r = { ...old }
                r.loading = false
                r.user = res.data
                return r
            })
        }).catch(err => {
            console.log(err)
            // if api call fails, return user to home screen
            history.push('/')
        })
    }, [id])

    return (
        <div className={classes.container}>
            {userDetails.loading ? null : <div className={classes.body}>
                <div className={classes.section}>
                    <UserInfo info={userDetails.user} />
                </div>
                <div className={classes.section}>
                    <UserPosts id={id} name={userDetails.user.name} />
                </div>
            </div>
            }
        </div>
    )
}

export default withRouter(UserDetails)