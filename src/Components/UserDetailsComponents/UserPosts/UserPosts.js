import React, { useEffect, useState } from 'react'
import { withRouter } from 'react-router'
import axiosInstance from '../../../Assets/Axios/AxiosInstance'
import SquareContainer from '../SquareContainer/SquareContainer'
import classes from './UserPosts.module.css'

const UserPosts = ({ id, name, history }) => {
    const [posts, setPosts] = useState([])

    // run on first render, as well as every render where the userID changes
    useEffect(() => {
        axiosInstance.get(`/posts?userId=${id}`).then(res => {
            // on success, set data to local state
            setPosts(res.data)
        }).catch(err => {
            // on error, redirect back to home page
            history.push('/')
        })
    }, [id])

    return (
        <div className={classes.container}>
            <h1>Posts By {name}</h1>
            <div className={classes.body}>
                {posts.map((i, k) => {
                    return <SquareContainer header={i.title} post key={k}>
                        <p>{i.body}</p>
                    </SquareContainer>

                })}
            </div>
        </div>
    )
}

export default withRouter(UserPosts)