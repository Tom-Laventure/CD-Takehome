import React from 'react'
import classes from './SquareContainer.module.css'

const SquareContainer = ({ children, header, post }) => {
    return (
        <div className={`${classes.container} ${post? classes.post: null}`}>
            <h4>{header}</h4>
            {children}
        </div>
    )
}

export default SquareContainer