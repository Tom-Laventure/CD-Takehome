import React from 'react'
import { Link } from 'react-router-dom'
import SquareContainer from '../SquareContainer/SquareContainer'
import classes from './UserInfo.module.css'

const UserInfo = ({ info }) => {

    return (
        <div className={classes.container}>
            <div className={classes.header}>
                <h1><Link to='/'>Users</Link> &gt; {info.name}</h1>
            </div>
            <div className={classes.body}>
                <SquareContainer header="Contact Info">
                    <p>Username: {info.username}</p>
                    <p>Email: <a href={`mailto:${info.email}`} >{info.email}</a></p>
                    <p>Phone: <a href={`tel:${info.phone}`} >{info.phone}</a></p>
                    <p>Website: <a target="_blank" href={`http://${info.website}`}>{info.website}</a></p>
                </SquareContainer>
                <SquareContainer header="Address">
                    <p>{info.address.suite}, {info.address.street}, {info.address.city}, {info.address.zipcode}</p>
                </SquareContainer>
                <SquareContainer header="Company">
                    <p>{info.company.name}</p>
                    <p>{info.company.bs}</p>
                    <p><i>"{info.company.catchPhrase}"</i></p>
                </SquareContainer>
            </div>
        </div>
    )
}

export default UserInfo