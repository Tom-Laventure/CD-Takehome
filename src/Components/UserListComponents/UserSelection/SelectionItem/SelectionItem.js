import React from 'react'
import { withRouter } from 'react-router-dom'
import { stopProp } from '../../../../Assets/Functions/StopPropagation'
import classes from './SelectionItem.module.css'

const SelectionItem = ({id, name, username, email, history}) => {

    return (
        <div className={classes.container} onClick={() => history.push(`/user-details/${id}`)}>
            <div className={classes.itemLeft}>
                <div className={classes.circle}></div>
            </div>
            <div className={classes.details}>
                <p>
                    {name}<br />
                    {username}
                </p>
                <div className={classes.email}>
                    <a href={`mailto:${email}`} onClick={(e) => stopProp(e)}>{email}</a>
                </div>
            </div>
        </div>
    )
}

export default withRouter(SelectionItem)