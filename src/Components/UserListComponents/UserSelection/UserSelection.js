import React, { useContext, useEffect, useState } from 'react'
import { StoreContext } from '../../../Store/StoreContext'
import SelectionItem from './SelectionItem/SelectionItem'
import classes from './UserSelection.module.css'

const UserSelection = () => {
    const { state } = useContext(StoreContext)
    const [userList, setUserList] = useState([])

    // to be run after users list has been retrieved, and whenever the central filter state is changed
    useEffect(() => {
        if (state.users.length > 0) {
            let users = [...filterList(state.users, state.filters)]
            setUserList(users)
        }
    }, [state.filters, state.users])

    const filterList = (users, filters) => {
        // If the search filter has input, filter the array object name, username and email for the search input, and then sort by the current dropdown selection
        if (filters.search !== '') {
            const search = filters.search.toUpperCase()
            return users.filter(i => { 
                return i.name.toUpperCase().includes(search) || i.username.toUpperCase().includes(search) || i.email.toUpperCase().includes(search)
            }).sort((a, b) => {
                const aa = a[filters.sortBy].toUpperCase() 
                const bb = b[filters.sortBy].toUpperCase()
                if (aa < bb) {
                    return -1;
                  }
                  if (aa > bb) {
                    return 1;
                  }
                  // strings must be equal
                  return 0;
            })
        // Else only sort the array by the current dropdown selection
        } else {
            return users.sort((a, b) => {
                const aa = a[filters.sortBy].toUpperCase() 
                const bb = b[filters.sortBy].toUpperCase()
                if (aa < bb) {
                    return -1;
                  }
                  if (aa > bb) {
                    return 1;
                  }
                  // strings must be equal
                  return 0;
            })
        }

    }
    
    return (
        <div className={classes.container}>
            {userList.map((i, k) => {
                return <SelectionItem
                    key={k}
                    name={i.name}
                    email={i.email}
                    username={i.username}
                    id={i.id}
                />
            })}
        </div>
    )
}

export default UserSelection