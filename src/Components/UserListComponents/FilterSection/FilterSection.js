import React, { useContext } from 'react'
import { Form } from 'react-bootstrap'
import { StoreContext } from '../../../Store/StoreContext'
import classes from './FilterSection.module.css'

const FilterSection = () => {
    const {actions} = useContext(StoreContext)

    const dropDownItems = [
        {label:"Name", value: "name"},
        {label:"Username", value: "username"},
        {label:"Email", value: "email"},
    ]

    // On input element change, update the filter object in the central state with the inputed value
    const formChange = (type, e) => {
        actions.setFilters({filterType: type, value: e.target.value})
    }

    return (
        <Form className={classes.container}>
            <Form.Group className={classes.inputContainer}>
                <Form.Label htmlFor="search">Search</Form.Label>
                <Form.Control id="search" size="sm" type="text" onChange={(e) => formChange('search', e)}/>
            </Form.Group>
            <Form.Group className={classes.inputContainer}>
                <Form.Label htmlFor="">Sort By</Form.Label>
                <Form.Control id="" size="sm" as="select" onChange={(e) => formChange('sortBy', e)}>
                    {dropDownItems.map((i, k) => {
                        return <option key={k} value={i.value}>{i.label}</option>
                    })}
                </Form.Control>
            </Form.Group>
        </Form>
    )
}

export default FilterSection