# CD-Takehome Project

## Description

A Project made to showcase my ability with ReactJS, Javascript, Modular css, and Bootstrap.

## Prerequisites

### Node JS

You can find the relevant documentation to install NodeJS here: https://nodejs.org/en/

### create-react-app

Use the following command to install create-react-app

### `npm install -g create-react-app`

## Clone and Run the Application in localhost

Clone the project onto your machine with the following command

### `git clone git@gitlab.com:Tom-Laventure/CD-Takehome.git`

To install all the npm packages, open a terminal in your project folder and type the following command

### `npm install`

To run the application, use the following command

### `npm start`

The application will run on localhost:3000






